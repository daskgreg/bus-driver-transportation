import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {

  constructor(private router: Router, private popoverCtrl: PopoverController) { }

  ngOnInit() {
  }

  pageNavigationToRetrieve(){
    this.router.navigate(['retrieve'])
  }
  pageNavigationToLogin(){
    this.router.navigate(['login'])
  }




}
