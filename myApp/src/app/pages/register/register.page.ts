import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
    first_name:any;
    last_name:any;
    license_number:any;
    birthday:any;
    password:any;
    mobile:any;
    country:any;
    location:any;
    address:any;
    zip:any;
    email:any;
    img_car_reg:any;
    img_car_ins:any;
    img_prof_pic:any;
    registerData:any;
  constructor
  (
    private router: Router,
    private http: HttpClient,
    private toastCtrl: ToastController,
    private LoadingController: LoadingController
  ) 
  { 

  }
  ngOnInit() {
  }

  register()
  {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
  
    let postData = {

      
      "first_name": this.first_name,
      "last_name": this.last_name,
      "license_number": this.license_number,
      "birthday": this.birthday,
      "password": this.password,
      "mobile": this.mobile,
      "country": 1,
      "location": this.location,
      "address": this.address,
      "zip": this.zip,
      "email": this.email,
      "img_car_reg": this.img_car_reg,
      "img_car_ins": this.img_car_ins,
      "img_prof_pic": this.img_prof_pic

    }

    this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/users/register?", postData, httpOptions)
    .subscribe(async data => {
      this.registerData = data;

      if(this.registerData.message == "Registration Successful"){
        let loader = await this.LoadingController.create({
          message: "Registration Successful"
        });

        loader.present();
        setTimeout(() => {
          loader.dismiss();
          this.router.navigate(['login']);
        }, 800);
      }
    })
    
  }
}
