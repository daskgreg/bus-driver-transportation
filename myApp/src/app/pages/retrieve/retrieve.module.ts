import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RetrievePageRoutingModule } from './retrieve-routing.module';

import { RetrievePage } from './retrieve.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RetrievePageRoutingModule
  ],
  declarations: [RetrievePage]
})
export class RetrievePageModule {}
