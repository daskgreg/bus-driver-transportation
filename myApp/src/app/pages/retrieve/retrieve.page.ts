import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-retrieve',
  templateUrl: './retrieve.page.html',
  styleUrls: ['./retrieve.page.scss'],
})
export class RetrievePage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  register()
  {
    this.router.navigate(['register']);
  }

  retrieveAccount()
  {
    this.router.navigate(['retrieve-account']);
  }

  retrievePassword()
  {
    this.router.navigate(['retrieve-password']);
  }

  login()
  {
    this.router.navigate(['login']);
  }

}
