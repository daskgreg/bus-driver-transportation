import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FakemapPage } from './fakemap.page';

const routes: Routes = [
  {
    path: '',
    component: FakemapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FakemapPageRoutingModule {}
