import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FakemapPageRoutingModule } from './fakemap-routing.module';

import { FakemapPage } from './fakemap.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FakemapPageRoutingModule
  ],
  declarations: [FakemapPage]
})
export class FakemapPageModule {}
