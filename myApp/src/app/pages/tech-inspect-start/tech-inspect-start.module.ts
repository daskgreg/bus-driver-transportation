import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TechInspectStartPageRoutingModule } from './tech-inspect-start-routing.module';

import { TechInspectStartPage } from './tech-inspect-start.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TechInspectStartPageRoutingModule
  ],
  declarations: [TechInspectStartPage]
})
export class TechInspectStartPageModule {}
