import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TechInspectStartPage } from './tech-inspect-start.page';

const routes: Routes = [
  {
    path: '',
    component: TechInspectStartPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TechInspectStartPageRoutingModule {}
