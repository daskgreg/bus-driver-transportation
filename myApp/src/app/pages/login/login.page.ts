import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  mobile:any;

  password:any;

  loginData:any; 

  constructor
  (
    private router: Router,
    private http: HttpClient,
    private toastCtrl: ToastController,
    private LoadingController: LoadingController
  ) 
  { 

  }

  ngOnInit() {
  }

  async sendPostRequest() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
  
    let postData = {
            "mobile": this.mobile,
            "password": this.password,
    }

    this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/users/login", postData, httpOptions)
      .subscribe(async data => {

        this.loginData = data;

        
        if(this.loginData.message == "Login Successful")
        {
           
          localStorage.setItem("driver_id",this.loginData.data[0].driver_id);
            localStorage.setItem("person_id",this.loginData.data[0].person_id);
            localStorage.setItem("stafftype_id",this.loginData.data[0].stafftype_id);
            localStorage.setItem("fname",this.loginData.data[0].fname);
            localStorage.setItem("lname",this.loginData.data[0].lname);
            localStorage.setItem("publishing_authority",this.loginData.data[0].publishing_authority);
            localStorage.setItem("password",this.loginData.data[0].password);
            localStorage.setItem("UserName",this.loginData.data[0].UserName);
            localStorage.setItem("LicenceNumber",this.loginData.data[0].LicenceNumber);

           let loader = await this.LoadingController.create({
              message: "Successfull Login"
            });

            loader.present();
            setTimeout(() => {
              loader.dismiss();
              this.router.navigate(['tech-inspect-start']);
            }, 800);
        }else{
          let loader = await this.LoadingController.create({
            message: "Mobile or Password is wrong"
          });

          loader.present();
          setTimeout(() => {
            loader.dismiss();
          }, 800);
        }
        
       }, error => {
        console.log(error);
      });
      
  }

  login()
  {
    this.router.navigate(['route']);
  }

}
