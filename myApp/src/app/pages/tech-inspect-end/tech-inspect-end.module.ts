import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TechInspectEndPageRoutingModule } from './tech-inspect-end-routing.module';

import { TechInspectEndPage } from './tech-inspect-end.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TechInspectEndPageRoutingModule,
  ],
  declarations: [TechInspectEndPage]
})
export class TechInspectEndPageModule {}
