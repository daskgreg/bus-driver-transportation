import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TechInspectEndPage } from './tech-inspect-end.page';

const routes: Routes = [
  {
    path: '',
    component: TechInspectEndPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TechInspectEndPageRoutingModule {}
