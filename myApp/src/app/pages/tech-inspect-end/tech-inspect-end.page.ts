import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { LoadingController, AlertController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import {formatDate} from '@angular/common';
@Component({
  selector: 'app-tech-inspect-end',
  templateUrl: './tech-inspect-end.page.html',
  styleUrls: ['./tech-inspect-end.page.scss'],
  providers:[DatePipe]
})
export class TechInspectEndPage implements OnInit {


  getInspectData:any;
  comment:any;
  selectedTech:any;
  id_hidden = false;
  selectedTechId:any;
  driver_id:any;
 myDate:any;
  
  constructor
  (
    private router: Router,
    private http: HttpClient,
    private toastCtrl: ToastController,
    private LoadingController: LoadingController,
    private datePipe: DatePipe,
    public alertController: AlertController
  ) 
  { 
    this.myDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
  }

  ngOnInit() {
 
    let today = Date.now();
    this.driver_id = localStorage.getItem("driver_id");

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {

      
        "lang": "eng"
  
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/techinspect/techInspect",postData, httpOptions)
    .subscribe(async data => {
      
      this.getInspectData = data;
      this.getInspectData = this.getInspectData.data;
      
    })
    
  }
  
  addTechInspect()
  {
    this.selectedTechId = this.selectedTech.substring(this.selectedTech.length -3);

    if(this.comment == undefined)
    {
      alert('Please fill comment');
      return 0;
    }
    if(this.selectedTech == undefined)
    {
      alert('Please fill Inspect');
      return 0;
    }
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {

      
        "vhc_plates":	"OPE5400",
        "chrbus_code":	2,
        "chrbus_sp_id":	1,
        "sp_code":	1,
        "fromd":	this.myDate,
        "tod":	this.myDate,
        "driver_id":	16,
        "checkpoint_id":	this.selectedTechId,
        "checkpoint_txt":	this.selectedTech,
        "lang":"eng",
        "checkpoint_status":	1,
        "comment":	this.comment,
        "chrbus_type":" ",
        "app_auth": 29,
        "app_id":1001
  
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/techinspect/addTechData",postData, httpOptions)
    .subscribe(async data => {
      
      this.getInspectData = data;
      
      if(this.getInspectData.message == "Succesful Request")
      {
        let loader = await this.LoadingController.create
        ({
          message: "Vehicle Tech Inspection created successfully"
        });

        loader.present();
        setTimeout
        (() => {
          loader.dismiss();
          this.alertController.create({
            header: 'Route has ended',
            subHeader: 'Thank you for your trip',
            message: 'Do you want to proceed and end your route?',
            buttons: [
              {
                text: 'Yes',
                handler: () => {
                  this.router.navigate(['route']);
                }
              },
              {
                text: 'No',
                handler: () => {
                  window.location.reload();
                }
              }
            ]
          }).then(res => {
            res.present();
          });
          
        }, 200);
      }
    })
  }

  goToProfile()
  {
    this.router.navigate(['profile']);
  }

  
}
