import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TechInspectPage } from './tech-inspect.page';

const routes: Routes = [
  {
    path: '',
    component: TechInspectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TechInspectPageRoutingModule {}
