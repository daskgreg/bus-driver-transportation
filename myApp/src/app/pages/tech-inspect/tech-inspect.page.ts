import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { LoadingController, AlertController } from '@ionic/angular';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-tech-inspect',
  templateUrl: './tech-inspect.page.html',
  styleUrls: ['./tech-inspect.page.scss'],
  
})
export class TechInspectPage implements OnInit {
  myDate:any;
  driver_id:any;
  getInspectData:any;
  getInspectDataArray: any = [];
  selectedInspect:any;
  select:any;
  updateAndDelete = false;
  selectItem = true;
  arrSelection:any;
  astod:any;
  asctext:any;
  asccomment:any;
  asctrans:any;
  selectedTech:any;
  id_hidden = false;
  selectedTechId:any;
  comment:any;
  getInspectDataTech:any;
  deleteTechInspection = false;
  updateTechInspection = false;
  constructor
  (
    private router: Router,
    private http: HttpClient,
    private toastCtrl: ToastController,
    private LoadingController: LoadingController,
    public alertController: AlertController
  ) 
  { 
    this.myDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
  }

  tech()
  {
    let today = Date.now();
    this.driver_id = localStorage.getItem("driver_id");

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {

      
        "lang": "eng"
  
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/techinspect/techInspect",postData, httpOptions)
    .subscribe(async data => {
      
      this.getInspectDataTech = data;
      this.getInspectDataTech = this.getInspectDataTech.data;
      
    })
  }
  ngOnInit() {
    this.tech();
    

    this.driver_id = localStorage.getItem("driver_id");

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {
        "chrbus_code": "2",
        "driver_id": this.driver_id
      }

      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/techinspect/getTechData",postData, httpOptions)
      .subscribe(async data => {
        
        this.getInspectData = data;
        this.getInspectData = this.getInspectData.data.slice().reverse();;
        

      })

  }

  updateOrDelete(select)
  {
     // this.selectItem = false;
      const jsonSelection = JSON.stringify(select);
      this.arrSelection = JSON.parse(jsonSelection);
      this.astod = this.arrSelection.fromd
      this.asctext = this.arrSelection.checkpoint_txt;
      this.asccomment = this.arrSelection.comment;
      this.asctrans = this.arrSelection.checkpoint_tran;
      this.selectItem = false;
      this.updateTechInspection = true;
 }
 del()
 {
  this.selectItem = false;
  this.updateTechInspection = false;
  this.alertController.create({
    header: 'Delete Inspection',
    subHeader: 'Beware lets confirm',
    message: 'Are you sure? Do you want to delete this Inspection?',
    buttons: [
      {
        text: 'Yes',
        handler: () => {
          this.deleteInspect();
        }
      },
      {
        text: 'No',
        handler: () => {
          window.location.reload();
        }
      }
    ]
  }).then(res => {
    res.present();
  });
 }
 up(select)
 {
   this.updateOrDelete(select);
 
 }
 deleteInspect()
 {
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Accept':'application/json'
      })
    };
    let postData = {
      "checkpoint_tran":this.asctrans
    }

    this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/techinspect/deleteTechData",postData, httpOptions)
    .subscribe(async data => {
      let loader = await this.LoadingController.create
        ({
          message: "SuccessFully Deleted"
        });

        loader.present();
        setTimeout
        (() => {
          loader.dismiss();
          window.location.reload();
        }, 200);

    })
 }

 update()
 {
  this.selectedTechId = this.selectedTech.substring(this.selectedTech.length -3);

  if(this.comment == undefined)
  {
    alert('Please fill comment');
    return 0;
  }
  if(this.selectedTech == undefined)
  {
    alert('Please fill Inspect');
    return 0;
  }
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Accept':'application/json'
      })
    };
    let postData = {

    
      "vhc_plates":	this.arrSelection.vhc_plates,
      "chrbus_code":	this.arrSelection.chrbus_code,
      "chrbus_sp_id":	this.arrSelection.chrbus_sp_id,
      "sp_code":	this.arrSelection.sp_code,
      "fromd":	this.arrSelection.fromd,
      "tod":	this.arrSelection.tod,
      "driver_id":	this.arrSelection.driver_id,
      "checkpoint_id":	this.arrSelection.checkpoint_id,
      "checkpoint_txt":	this.selectedTech,
      "lang": this.arrSelection.lang,
      "checkpoint_status":	this.arrSelection.checkpoint_status,
      "comment":	this.comment,
      "chrbus_type":this.arrSelection.chrbus_type,
      "app_auth": this.arrSelection.app_auth,
      "app_id":this.arrSelection.app_id,
      "checkpoint_tran": this.arrSelection.checkpoint_tran,
     
    
    }
    this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/techinspect/updateTechData",postData, httpOptions)
  .subscribe(async data => {
    
    this.getInspectData = data;
    
    if(this.getInspectData.message == "Succesful Request")
    {
      let loader = await this.LoadingController.create
      ({
        message: "Vehicle Tech Inspection created successfully"
      });

      loader.present();
      setTimeout
      (() => {
        loader.dismiss();
        this.alertController.create({
          header: 'Update Inspection',
          subHeader: 'Beware lets confirm',
          message: 'Are you sure? Do you want to update this Inspection?',
          buttons: [
            {
              text: 'Yes',
              handler: () => {
                window.location.reload();
              }
            },
            {
              text: 'No',
              handler: () => {
                window.location.reload();
              }
            }
          ]
        }).then(res => {
          res.present();
        });
      }, 200);
    }
  })
 }

}
