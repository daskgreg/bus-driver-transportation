import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TechInspectPageRoutingModule } from './tech-inspect-routing.module';

import { TechInspectPage } from './tech-inspect.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TechInspectPageRoutingModule
  ],
  declarations: [TechInspectPage]
})
export class TechInspectPageModule {}
