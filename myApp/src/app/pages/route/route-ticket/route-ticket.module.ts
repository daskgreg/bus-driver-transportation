import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouteTicketPageRoutingModule } from './route-ticket-routing.module';

import { RouteTicketPage } from './route-ticket.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouteTicketPageRoutingModule
  ],
  declarations: [RouteTicketPage]
})
export class RouteTicketPageModule {}
