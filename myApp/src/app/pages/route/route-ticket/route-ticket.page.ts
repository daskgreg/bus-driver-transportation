import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSearchbar } from '@ionic/angular';

@Component({
  selector: 'app-route-ticket',
  templateUrl: './route-ticket.page.html',
  styleUrls: ['./route-ticket.page.scss'],
})
export class RouteTicketPage implements OnInit {


  @ViewChild('search', {static:false}) search: IonSearchbar;

  public list: Array<Object> = [];

  private searchedItem:any;

  isSearchedArrival = false;

  arrivalStatus = false;

  selectedDeparture:any;
  
  departure:any;
  
  searchBar = true;

  passengerDetails = false;

  passengerDetailsPlus = true;

  passengerDetailsRemove = false;

  extraoptions = false;

  extraoptionsPlus = true;

  extraoptionsRemove = false;

  ticketsPage = true;

  nextTicketsPage = false;

  continueNextPage = false;

  constructor(private router:Router) {

    this.list = [
      { title: "Athens"},
      { title: "Lamia" },
      { title: "Volos" },
      { title: "Thessaloniki"}
    ];

    this.searchedItem = this.list;
   }

  ngOnInit() {
  }
  
  ionViewDidEnter(){
    setTimeout(() =>
    {
      this.search.setFocus();
    });
  }

  searchEvent(event){
    
    const val = event.target.value;

    this.searchedItem = this.list;
    this.isSearchedArrival = true;
    if(val && val.trim() != '')
    {
     this.searchedItem = this.searchedItem.filter((item:any) => {
       return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
     }) 
    }
  }

  getDepartures(item:any)
  {
    this.departure = item.title;
    this.arrivalStatus = true;
    this.searchBar = false;
    this.isSearchedArrival = false;
  }

  showSearch()
  {
    this.arrivalStatus = false;
    this.searchBar = true;
  }

  showPassengerDetails()
  {

    if(this.passengerDetails == false)
    {
      this.passengerDetailsPlus = false;
      this.passengerDetailsRemove = true;
      this.passengerDetails = true;
    }else {
      this.passengerDetailsPlus = true;
      this.passengerDetailsRemove = false;
      this.passengerDetails = false;
    }
  
  }

  showExtraOptions()
  {

    if(this.extraoptions == false)
    {
      this.extraoptionsPlus = false;
      this.extraoptionsRemove = true;
      this.extraoptions = true;
    }else 
    {
      this.extraoptionsPlus = true;
      this.extraoptionsRemove = false;
      this.extraoptions = false;
    }
  
  }

  continue()
  {
    if(this.ticketsPage == true)
    {
      this.ticketsPage = false;
    }

    if(this.nextTicketsPage == false)
    {
      this.nextTicketsPage = true;
    }
  }

  continueNext()
  {
    if( this.continueNextPage == false)
    {
      this.continueNextPage = true;
      this.nextTicketsPage = false;
    }
  }

 


}
