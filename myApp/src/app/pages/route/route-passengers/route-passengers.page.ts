import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController,LoadingController } from '@ionic/angular';
import { isWithinInterval,isBefore } from 'date-fns';

import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
@Component({
  selector: 'app-route-passengers',
  templateUrl: './route-passengers.page.html',
  styleUrls: ['./route-passengers.page.scss'],
})
export class RoutePassengersPage implements OnInit {
  showPassengerDetails: {[key: number]: boolean} = {};
  vehicle_map_id:any;
  passengers:any;
  constructor(
    private router:Router,
    public navCtrl: NavController,
    public toastCtrl:ToastController,
    private http: HttpClient,
    private LoadingController: LoadingController
    ) { }

  ngOnInit() {
    this.vehicle_map_id = localStorage.getItem("vehicle_map_id");
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {      
        "vehicle_map_id": this.vehicle_map_id,
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/routelist/passengers",postData, httpOptions)
      .subscribe(async data => {
        this.passengers = data;
        this.passengers = this.passengers.data;

      })
  }

  showPassengers(index: number)
  {
    if(this.showPassengerDetails[index] == false){
      this.showPassengerDetails[index] = true;
    }else{
      this.showPassengerDetails[index] = false;
    }
  }
}
