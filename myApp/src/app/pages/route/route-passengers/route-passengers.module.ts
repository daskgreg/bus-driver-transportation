import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoutePassengersPageRoutingModule } from './route-passengers-routing.module';

import { RoutePassengersPage } from './route-passengers.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoutePassengersPageRoutingModule
  ],
  declarations: [RoutePassengersPage]
})
export class RoutePassengersPageModule {}
