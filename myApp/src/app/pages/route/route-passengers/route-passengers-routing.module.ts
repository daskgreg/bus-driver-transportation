import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoutePassengersPage } from './route-passengers.page';

const routes: Routes = [
  {
    path: '',
    component: RoutePassengersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutePassengersPageRoutingModule {}
