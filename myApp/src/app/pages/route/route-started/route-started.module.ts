import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouteStartedPageRoutingModule } from './route-started-routing.module';

import { RouteStartedPage } from './route-started.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouteStartedPageRoutingModule
  ],
  declarations: [RouteStartedPage]
})
export class RouteStartedPageModule {}
