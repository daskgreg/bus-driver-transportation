import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouteStartedPage } from './route-started.page';

const routes: Routes = [
  {
    path: '',
    component: RouteStartedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteStartedPageRoutingModule {}
