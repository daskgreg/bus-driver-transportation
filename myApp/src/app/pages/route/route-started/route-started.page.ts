import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController,LoadingController } from '@ionic/angular';
import { isWithinInterval,isBefore } from 'date-fns';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
@Component({
  selector: 'app-route-started',
  templateUrl: './route-started.page.html',
  styleUrls: ['./route-started.page.scss'],
})
export class RouteStartedPage implements OnInit {
  selectItemR:any;
  custom:any;
  customArrival:any;
  customDeparture:any;

   constructor(
    private router:Router,
    public navCtrl: NavController,
    public toastCtrl:ToastController,
    private http: HttpClient,
    private LoadingController: LoadingController
    ) { }

  ngOnInit() {
    this.selectItemR = localStorage.getItem("route_selection");
    this.selectItemR = JSON.parse(this.selectItemR);
    localStorage.setItem("vehicle_map_id",this.selectItemR.vehicle_map_id);

    if(this.selectItemR.type == "CUST")
    {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Accept':'application/json'
          })
        };
        let postData = {      
          "route_id": this.selectItemR.ServiceCode,
        }
        this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/routelist/custom",postData, httpOptions)
        .subscribe(async data => {
          this.custom = data;
          this.custom = this.custom.data;
         const maplonglat = JSON.stringify(this.custom);
         localStorage.setItem("MAPLONGLAT",maplonglat);
         this.customArrival = this.custom[0].pickup_address;
         this.customDeparture = this.custom[this.custom.length-1].pickup_address; 

        })
    }else
    {
       console.log(this.selectItemR);
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':'application/json',
          'Accept':'application/json'
          })
        };
        let postData = {      
          "chrbus_code": this.selectItemR.ServiceCode,
        }
        this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/routelist/fixed",postData, httpOptions)
        .subscribe(async data => {
          this.custom = data;
          this.custom = this.custom.data;
         const maplonglat = JSON.stringify(this.custom);
         localStorage.setItem("MAPLONGLAT",maplonglat);
         this.customArrival = this.custom[0].cty_name;
         this.customDeparture = this.custom[this.custom.length-1].cty_name; 

        })
    }

   
  }
  startRoute()
  {
    this.router.navigate(['route-map']);
  }
  routePassengers()
  {
    this.router.navigate(['route-passengers']);
  }
  map()
  {
    this.router.navigate(['route-map']);
  }
  wallet()
  {
    this.router.navigate(['route-wallet']);
  }
}
