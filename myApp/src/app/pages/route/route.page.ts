import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController,LoadingController } from '@ionic/angular';
import { isWithinInterval,isBefore } from 'date-fns';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
@Component({
  selector: 'app-route',
  templateUrl: './route.page.html',
  styleUrls: ['./route.page.scss'],
})
export class RoutePage implements OnInit {

  startDate;
  endDate;
  invalidSelection: boolean = false;
  driver_id:any;
  getDatesDriver:any;
  customArr:any;
  custom:any;
  customArrival:any;
  customDeparture:any;

  fixed:any;
  fixedArrival:any;
  fixedDeparture:any;

  dateSelected = false;
  fixSelected = false;
  custSelected = false;
  select:any;
  selectItemR:any;
  data = [
    {
      routeArrival:'Hrakleio Crete',
      routeDeparture: 'Thessaloniki',
      date: '2021-01-01'
    },
    {
      routeArrival:'Hrakleio Crete',
      routeDeparture: 'Larisa',
      date: '2021-04-01'
    },
    {
      routeArrival:'Hrakleio Crete',
      routeDeparture: 'Lamia',
      date: '2021-03-01'
    },
    {
      routeArrival:'Hrakleio Crete',
      routeDeparture: 'Athens',
      date: '2021-02-01'
    }
  ]

  filtered = [...this.data];

  constructor(
    private router:Router,
    public navCtrl: NavController,
    public toastCtrl:ToastController,
    private http: HttpClient,
    private LoadingController: LoadingController
    ) { }

  ngOnInit() {
  }

  selectItemRoute(select)
  {
      select = JSON.stringify(select);
      localStorage.setItem("route_selection",select);
      this.selectItemR = localStorage.getItem("route_selection");
     
        this.router.navigate(['route-started']);
      

  }
  async loadResults()
  {
    this.fixSelected = true;
    this.custSelected = true;
    this.driver_id = localStorage.getItem("driver_id");
    this.startDate = this.startDate.split('T')[0];
    this.endDate = this.endDate.split('T')[0];

    if(!this.startDate || !this.endDate)
    {
      return
    }

    if(isBefore(this.endDate,this.startDate))

    {
      (await this.toastCtrl.create({
        message: 'Note: Start Date cannot be set in the future',
        position: 'bottom',
      })).present();

      this.filtered = [];
      this.invalidSelection = true;

    }
    // const startDate = new Date(this.startDate);
    // const endDate = new Date(this.endDate);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {      
        "driver_id": this.driver_id,
        "from_date":this.startDate,
        "to_date":this.endDate  
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/routelist/driver",postData, httpOptions)
      .subscribe(async data => {
        
        this.dateSelected = true;


        this.getDatesDriver = data;
        this.getDatesDriver = this.getDatesDriver.data

        for( var i = 0; i<= this.getDatesDriver.length; i++)
        {
          if(this.getDatesDriver[i].type == "CUST")
          {
            const httpOptions = {
              headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Accept':'application/json'
                })
              };
              let postData = {      
                "route_id": this.getDatesDriver[i].ServiceCode,
              }
              this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/routelist/custom",postData, httpOptions)
              .subscribe(async data => {
                this.custom = data;
                this.custom = this.custom.data;

               this.customArrival = this.custom[0].pickup_address;
               this.customDeparture = this.custom[this.custom.length-1].pickup_address; 

              })


            this.customArr = this.getDatesDriver[i].type;
          }
          if(this.getDatesDriver[i].type == "FIX")
        {
          const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Accept':'application/json'
              })
            };
            let postData = {      
              "chrbus_code": this.getDatesDriver[i].ServiceCode,
            }
            this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/routelist/fixed",postData, httpOptions)
            .subscribe(async data => {
              this.fixed = data;
              this.fixed = this.fixed.data;

             this.fixedArrival = this.fixed[0].cty_name;
             this.fixedDeparture = this.fixed[this.fixed.length-1].cty_name; 

            })
        }
        }
        
      })

    this.filtered = this.getDatesDriver.filter(item =>{
      return isWithinInterval(new Date(item.Assignment_To_Date.split('T')[0]), { start: this.startDate, end:this.endDate})
    })

  }

  showFixed()
  {
    this.fixSelected = true;
    this.custSelected = false;
  }
  showCustom()
  {
  
    this.fixSelected = false;
    this.custSelected = true;
  }
  showAll()
  {
    this.fixSelected = true;
    this.custSelected = true;
  }
  goToProfile(){
    this.router.navigate(['profile']);
  }
  takeRoute()
  {
    this.router.navigate(['route-started']);
  }

}
