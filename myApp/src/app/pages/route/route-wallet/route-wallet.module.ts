import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouteWalletPageRoutingModule } from './route-wallet-routing.module';

import { RouteWalletPage } from './route-wallet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouteWalletPageRoutingModule
  ],
  declarations: [RouteWalletPage]
})
export class RouteWalletPageModule {}
