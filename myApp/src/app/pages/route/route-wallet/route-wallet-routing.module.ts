import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouteWalletPage } from './route-wallet.page';

const routes: Routes = [
  {
    path: '',
    component: RouteWalletPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteWalletPageRoutingModule {}
