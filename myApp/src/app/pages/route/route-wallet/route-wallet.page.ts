import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController,LoadingController, AlertController } from '@ionic/angular';
import { isWithinInterval,isBefore } from 'date-fns';
import {formatDate} from '@angular/common';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
@Component({
  selector: 'app-route-wallet',
  templateUrl: './route-wallet.page.html',
  styleUrls: ['./route-wallet.page.scss'],
})
export class RouteWalletPage implements OnInit {
  transactionINCOME = false;
  transactionCOST = false;
  selectTransaction = false;
  cost:any;
  income:any;
  select:any;
  selectItemR:any;
  driver_id:any;
  selectTypeOfCost:any;
  tran_type_id:any;
  moneyPaid:any;
  showAllTrans:any;
  showTransaction = false;
  hideElement = true;
  arrSelection:any;
  arrSelectionUpdate:any;
  transactionCOSTupdate:any;
  costUpdate:any;
  selectTypeOfCostUpdate:any;
  moneyPaidUpdate:any;
  tran_type_idUpdate:any;
  selectTypeOfCostUpdateID:any;
  myDate:any;
  transactionINCOMEupdate = false;
  incomeUpdate:any;
  moneyPaidUpdateIncome:any;
  constructor(
    private router:Router,
    public navCtrl: NavController,
    public toastCtrl:ToastController,
    private http: HttpClient,
    private LoadingController: LoadingController,
    public alertController: AlertController
    ) {
      this.myDate = formatDate(new Date(), 'yyyy-MM-dd', 'en');
     }

  ngOnInit() {
    this.selectItemR = localStorage.getItem("route_selection");
    this.selectItemR = JSON.parse(this.selectItemR);
    this.driver_id = localStorage.getItem("driver_id");
  }

  ticket()
  {
    this.router.navigate(['route-ticket']);
  }
  updateOrDelete(select)
  {
     // this.selectItem = false;
      const jsonSelection = JSON.stringify(select);
      this.arrSelectionUpdate = JSON.parse(jsonSelection);
    this.updateTransaction();

 }
 up(select)
 {
   this.updateOrDelete(select);

 }
 del()
 {
  this.alertController.create({
    header: 'Delete Inspection',
    subHeader: 'Beware lets confirm',
    message: 'Are you sure? Do you want to delete this Inspection?',
    buttons: [
      {
        text: 'Yes',
        handler: () => {
          this.deleteTransaction();
        }
      },
      {
        text: 'No',
        handler: () => {
          window.location.reload();
        }
      }
    ]
  }).then(res => {
    res.present();
  });
 }
 deleteTransaction()
 {
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Accept':'application/json'
      })
    };
    let postData = {
      "driver_id":	this.arrSelectionUpdate.driver_id,
      "tran_id":	this.arrSelectionUpdate.tran_id
    }

    this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/deleteTransaction",postData, httpOptions)
    .subscribe(async data => {
      let loader = await this.LoadingController.create
        ({
          message: "SuccessFully Deleted"
        });

        loader.present();
        setTimeout
        (() => {
          loader.dismiss();
          window.location.reload();
        }, 200);

    })
 }
 updateTransaction()
 {
   if(this.arrSelectionUpdate.tran_type == "COST")
   {
   
    
    this.showTransaction = false;
    this.selectTransaction = false;
    this.transactionCOSTupdate= true;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {      
        "lang": "eng",
        "tran_type":"COST"
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/getWalletData",postData, httpOptions)
      .subscribe(async data => {

        this.costUpdate = data;
        this.costUpdate = this.costUpdate.data;
      })

   }

   if(this.arrSelectionUpdate.tran_type == "INCOME")
   {
    this.showTransaction = false;
    this.selectTransaction = false;
    this.transactionINCOMEupdate = true;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {      
        "lang": "eng",
        "tran_type":"INCOME"
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/getWalletData",postData, httpOptions)
      .subscribe(async data => {

        this.incomeUpdate = data;
        this.incomeUpdate = this.incomeUpdate.data;
        this.incomeUpdate = this.incomeUpdate[0].tran_type_descr_eng;
      })
   }
 }
  showAllTransactions()
  {
    this.hideElement = false;
    this.showTransaction = true;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {      
      
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterbus/wallet/getTransactions",postData, httpOptions)
      .subscribe(async data => {
        this.showAllTrans = data;
        this.showAllTrans = this.showAllTrans.data;
      })
  }
  transaction()
  {
    this.alertController.create({
      header: 'Transactions',
      subHeader: 'Which transaction do youwant',
      message: 'Select transaction method',
      buttons: [
        {
          text: 'New',
          handler: () => {
            this.selectTransaction = true;
          }
        },
        {
          text: 'See All',
          handler: () => {
            this.showAllTransactions();
          }
        }
      ]
    }).then(res => {
      res.present();
    });
     
  }
  ionChangeCost()
  {


   
    this.functionTransaction();
  }

  functionTransaction()
  {
    if(this.select == "COST")
    {
      this.selectTransaction = false;
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Accept':'application/json'
          })
        };
        let postData = {      
          "lang": "eng",
          "tran_type":this.select
        }
        this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/getWalletData",postData, httpOptions)
        .subscribe(async data => {

          this.cost = data;
          this.cost = this.cost.data;

        })
      this.transactionCOST = true;
    }else if(this.select == "INCOME")
    {
      this.selectTransaction = false;
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Accept':'application/json'
          })
        };
        let postData = {      
          "lang": "eng",
          "tran_type":this.select
        }
        this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/getWalletData",postData, httpOptions)
        .subscribe(async data => {

          this.income = data;
          this.income = this.income.data;
          this.tran_type_id = this.income[0].tran_type_id;
        })
      this.transactionINCOME = true;
    }
  }
  selectCostUpdate()
  {
    this.selectTypeOfCostUpdateID = this.selectTypeOfCostUpdate.substring(this.selectTypeOfCostUpdate.length-1)
  }
  selectCost()
  {

    if(!this.selectTypeOfCost){
    }
  }
  getPaidUpdate()
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {
        "tran_id":this.arrSelectionUpdate.tran_id,      
        "tran_date":this.myDate,
        "tran_type": this.arrSelectionUpdate.tran_type,
        "tran_type_id":	this.selectTypeOfCostUpdateID,
        "srv_type": this.arrSelectionUpdate.srv_type,
        "sp_code":	this.arrSelectionUpdate.sp_code,
        "fromd":	this.arrSelectionUpdate.fromd,
        "tod":	this.arrSelectionUpdate.tod,
        "srv_subtype": this.arrSelectionUpdate.srv_subtype,
        "driver_id":	this.arrSelectionUpdate.driver_id,
        "credit":	this.arrSelectionUpdate.credit,
        "debit":	this.moneyPaidUpdate,
        "sp_id":	this.arrSelectionUpdate.sp_id,
        "srv_code": this.arrSelectionUpdate.srv_code,
        "app_auth": this.arrSelectionUpdate.app_auth,
        "app_id": this.arrSelectionUpdate.app_id
      }
           
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/updateTransaction",postData, httpOptions)
      .subscribe(async data => {
        window.location.reload();

      })
  }
  sendPay()
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {      
        "tran_type":this.select,
        "tran_type_id":	this.tran_type_id,
        "srv_type": this.selectItemR.Service,
        "sp_code":	1,
        "fromd":	this.selectItemR.Assignment_From_Date,
        "tod":	this.selectItemR.Assignment_To_Date,
        "srv_subtype": this.selectItemR.type,
        "driver_id":	this.driver_id,
        "credit":	0,
        "debit":	this.moneyPaid,
        "sp_id":	1,
        "srv_code": this.selectItemR.ServiceCode,
        "app_auth": this.selectItemR.app_auth,
        "app_id": this.selectItemR.app_id
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/addTransaction",postData, httpOptions)
      .subscribe(async data => {

        this.income = data;
        this.income = this.income.data;

      })
  }

  getPaidIncome()
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {      
        "tran_id":this.arrSelectionUpdate.tran_id,      
        "tran_date":this.arrSelectionUpdate.tran_date,
        "tran_type": this.arrSelectionUpdate.tran_type,
        "tran_type_id":	this.arrSelectionUpdate.tran_type_id,
        "srv_type": this.arrSelectionUpdate.srv_type,
        "sp_code":	this.arrSelectionUpdate.sp_code,
        "fromd":	this.arrSelectionUpdate.fromd,
        "tod":	this.arrSelectionUpdate.tod,
        "srv_subtype": this.arrSelectionUpdate.srv_subtype,
        "driver_id":	this.arrSelectionUpdate.driver_id,
        "credit":	this.arrSelectionUpdate.credit,
        "debit":	this.moneyPaidUpdateIncome,
        "sp_id":	this.arrSelectionUpdate.sp_id,
        "srv_code": this.arrSelectionUpdate.srv_code,
        "app_auth": this.arrSelectionUpdate.app_auth,
        "app_id": this.arrSelectionUpdate.app_id
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/updateTransaction",postData, httpOptions)
      .subscribe(async data => {

        this.income = data;
        this.income = this.income.data;

        window.location.reload();

      })
  }

  getPaid()
  {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
      let postData = {      
        "tran_type":this.select,
        "tran_type_id":	this.tran_type_id,
        "srv_type": this.selectItemR.Service,
        "sp_code":	1,
        "fromd":	this.selectItemR.Assignment_From_Date,
        "tod":	this.selectItemR.Assignment_To_Date,
        "srv_subtype": this.selectItemR.type,
        "driver_id":	this.driver_id,
        "credit":	0,
        "debit":	this.moneyPaid,
        "sp_id":	1,
        "srv_code": this.selectItemR.ServiceCode,
        "app_auth": this.selectItemR.app_auth,
        "app_id": this.selectItemR.app_id
      }
      this.http.post("http://api.staging.travelsoft.gr/api/v1/charterBus/wallet/addTransaction",postData, httpOptions)
      .subscribe(async data => {

        this.income = data;
        this.income = this.income.data;

      })
  }
}
