import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouteHistoryPageRoutingModule } from './route-history-routing.module';

import { RouteHistoryPage } from './route-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouteHistoryPageRoutingModule
  ],
  declarations: [RouteHistoryPage]
})
export class RouteHistoryPageModule {}
