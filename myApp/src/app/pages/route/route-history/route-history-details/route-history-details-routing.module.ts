import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouteHistoryDetailsPage } from './route-history-details.page';

const routes: Routes = [
  {
    path: '',
    component: RouteHistoryDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteHistoryDetailsPageRoutingModule {}
