import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouteHistoryDetailsPageRoutingModule } from './route-history-details-routing.module';

import { RouteHistoryDetailsPage } from './route-history-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouteHistoryDetailsPageRoutingModule
  ],
  declarations: [RouteHistoryDetailsPage]
})
export class RouteHistoryDetailsPageModule {}
