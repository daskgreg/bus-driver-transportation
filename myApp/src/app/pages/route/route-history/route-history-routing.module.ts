import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RouteHistoryPage } from './route-history.page';

const routes: Routes = [
  {
    path: '',
    component: RouteHistoryPage
  },
  {
    path: 'route-history-details',
    loadChildren: () => import('./route-history-details/route-history-details.module').then( m => m.RouteHistoryDetailsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteHistoryPageRoutingModule {}
