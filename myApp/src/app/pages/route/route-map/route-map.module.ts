import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { IonicModule } from '@ionic/angular';

import { RouteMapPageRoutingModule } from './route-map-routing.module';

import { RouteMapPage } from './route-map.page';
import { ClickColorDirective } from './click-color.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouteMapPageRoutingModule,
  ],
  declarations: [RouteMapPage,ClickColorDirective]
})
export class RouteMapPageModule {}
