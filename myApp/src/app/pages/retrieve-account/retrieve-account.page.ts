import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-retrieve-account',
  templateUrl: './retrieve-account.page.html',
  styleUrls: ['./retrieve-account.page.scss'],
})
export class RetrieveAccountPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  retrieve()
  {
    this.router.navigate(['verification']);
  }
}
