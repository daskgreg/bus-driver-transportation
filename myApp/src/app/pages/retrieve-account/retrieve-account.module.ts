import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RetrieveAccountPageRoutingModule } from './retrieve-account-routing.module';

import { RetrieveAccountPage } from './retrieve-account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RetrieveAccountPageRoutingModule
  ],
  declarations: [RetrieveAccountPage]
})
export class RetrieveAccountPageModule {}
