import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor
  (
    private router: Router,
    private http: HttpClient,
    private toastCtrl: ToastController,
    private LoadingController: LoadingController
  ) 
  { 

  }

  ngOnInit() 
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Accept':'application/json'
        })
      };
  
    let postData = {
      "person_id": 3744,
      "first_name": "firstname",
      "last_name": "lastname"
    } 

    this.http.get("http://api.staging.travelsoft.gr/api/v1/charterBus/users/profile" , {})
    .subscribe(async data => {
     
    })
  }
 
}
