import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/main/main.module').then(m => m.MainPageModule)
  },
  {
    path: 'homepage',
    loadChildren: () => import('./pages/homepage/homepage.module').then( m => m.HomepagePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./pages/main/main.module').then( m => m.MainPageModule)
  },
  {
    path: 'retrieve-password',
    loadChildren: () => import('./pages/retrieve-password/retrieve-password.module').then( m => m.RetrievePasswordPageModule)
  },
  {
    path: 'retrieve-account',
    loadChildren: () => import('./pages/retrieve-account/retrieve-account.module').then( m => m.RetrieveAccountPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'mobile-verification',
    loadChildren: () => import('./pages/mobile-verification/mobile-verification.module').then( m => m.MobileVerificationPageModule)
  },
  {
    path: 'route',
    loadChildren: () => import('./pages/route/route.module').then( m => m.RoutePageModule)
  },
  {
    path: 'route-passengers',
    loadChildren: () => import('./pages/route/route-passengers/route-passengers.module').then( m => m.RoutePassengersPageModule)
  },
  {
    path: 'route-wallet',
    loadChildren: () => import('./pages/route/route-wallet/route-wallet.module').then( m => m.RouteWalletPageModule)
  },
  {
    path: 'route-map',
    loadChildren: () => import('./pages/route/route-map/route-map.module').then( m => m.RouteMapPageModule)
  },
  {
    path: 'route-ticket',
    loadChildren: () => import('./pages/route/route-ticket/route-ticket.module').then( m => m.RouteTicketPageModule)
  },
  {
    path: 'route-history',
    loadChildren: () => import('./pages/route/route-history/route-history.module').then( m => m.RouteHistoryPageModule)
  },
  {
    path: 'route-history-details',
    loadChildren: () => import('./pages/route/route-history/route-history-details/route-history-details.module').then( m => m.RouteHistoryDetailsPageModule)
  },
  {
    path: 'retrieve',
    loadChildren: () => import('./pages/retrieve/retrieve.module').then( m => m.RetrievePageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'verification',
    loadChildren: () => import('./pages/verification/verification.module').then( m => m.VerificationPageModule)
  },
  {
    path: 'route-started',
    loadChildren: () => import('./pages/route/route-started/route-started.module').then( m => m.RouteStartedPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'tech-inspect-start',
    loadChildren: () => import('./pages/tech-inspect-start/tech-inspect-start.module').then( m => m.TechInspectStartPageModule)
  },
  {
    path: 'tech-inspect-end',
    loadChildren: () => import('./pages/tech-inspect-end/tech-inspect-end.module').then( m => m.TechInspectEndPageModule)
  },
  {
    path: 'tech-inspect',
    loadChildren: () => import('./pages/tech-inspect/tech-inspect.module').then( m => m.TechInspectPageModule)
  },
  {
    path: 'fakemap',
    loadChildren: () => import('./pages/fakemap/fakemap.module').then( m => m.FakemapPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
